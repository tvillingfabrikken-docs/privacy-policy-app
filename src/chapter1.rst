================
Title
================

This chapter will include this and that.

Here is a figure with caption:

.. figure:: ./figures/tvillingfabrikkenLogoDarkRing.png
  :name: logo
  :width: 150px
  :target: https://tvillingfabrikken.no
  :align: center

  *This is a caption for the figure above. If you press the figure, you go to tvillingfabrikken.no*

Subtitle
----------------

This subtitle will include this and that. It can even include some code, in this case python:

.. code-block:: python

    #This code might not be easy to understand, but it basically prints out the message "Hello world!"
    print("Hello world!")

We can show you some C# code as well. For good measures I will highlight ``Hello``, which is a class.

.. code-block:: c#

    // Hello World! program
    namespace HelloWorld
    {
        class Hello {         
            static void Main(string[] args)
            {
                System.Console.WriteLine("Hello World!");
            }
        }
    }


Subsubtitle
****************

This subsubtitle has some information.

.. note::
   This is note text. Use a note for information you want the user to
   pay particular attention to.

.. important::
    This is very important.

    It's so important that it has two paragraphs.

.. warning::
    BTW, don't forget this warning, it might cause you trouble.

.. error::
    Woops, this is an error. I don't know what to use it for, but the red box looks cool.

.. seealso::
    See this also

This is another subtitle
------------------------

We can write complex equations:

.. math::
    \begin{bmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        1 & T & T^2 & T^3 \\
        0 & 1 & 2\cdot T & 3\cdot T^2
    \end{bmatrix} \cdot
    \begin{bmatrix}
        a_0\\
        a_1\\
        a_2\\
        a_3
    \end{bmatrix} = 
    \begin{bmatrix}
        \theta_0\\
        \dot\theta_0\\
        \theta_1\\
        \dot\theta_1
    \end{bmatrix}
